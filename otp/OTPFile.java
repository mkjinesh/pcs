/***
 *Encrypt or decrypt the file using one time pad
 *@author Jinesh M.K
 *Date: 20/1/13
 */
package pcs.otp;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;


public class OTPFile extends OTP{
	
	private static final int BUFFER_SIZE= 2000;
	private static final String FILE_ERROR = "File Error";
	/***
	 * Encrypt a input file using key and output to output file using OTP 
	 * @param inputFileName
	 * @param keyFile
	 * @param outputFileName
	 */
	public void encryptAndDecryptFile(String inputFileName,String keyFile, String outputFileName){
		BufferedInputStream  fileIn = null,fileKey=null;
		BufferedOutputStream fileOut=null;
		byte[] input=null,key=null;
		long size=0;
		try{
			File inputFile=new File(inputFileName);
			System.out.println(inputFile.length());
			if(inputFileName.length()<=new File(keyFile).length()){ //Checking input file and key file size
				
				fileIn=new BufferedInputStream(new FileInputStream(inputFileName));
				fileKey=new BufferedInputStream(new FileInputStream(keyFile));
				fileOut = new BufferedOutputStream(new FileOutputStream(outputFileName));
				input=new byte[BUFFER_SIZE]; //Buffer to hold data from input file
				key=new byte[BUFFER_SIZE];   //Buffer to hold data from key file
				//fileOut.write(oneTimePad(input, key));
				
				while(size<inputFile.length()){ //Read entire content
					int readSize=fileIn.read(input);
					size+=readSize;
					fileKey.read(key);
					fileOut.write(oneTimePad(input, key,readSize));
					fileOut.flush();
				}
				
				fileOut.close();
				fileIn.close();
				fileKey.close();
				System.out.println("Output Stored in "+outputFileName);
			}
			else{
				System.out.println(OTP.SIZE_ERROR);
				System.exit(1);
			}
		} catch(Exception e){
			System.err.println(FILE_ERROR);
			e.printStackTrace();		
		}
	}

}
