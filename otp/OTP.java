/****
 * The one-time pad (OTP) is a type of encryption which has been proven to be 
 * impossible to crack if used correctly. 
 * Each bit or character from the plaintext is encrypted by a modular addition 
 * with a bit or character from a secret random key of the same length(Here used a bitwise XOR)
 * as the plaintext, resulting in a ciphertext.
 * @author Jinesh M.K
 */

package pcs.otp;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;


class OTP {
	
	/* Buffer Size for file reading */
	static final String SIZE_ERROR = "INPUT and key length mis-matching" ;
	static final String KEY_MSG=" Random key file generated and stored in ";
	/***
	 * 	Bit wise xor of input and key
     * @param input
     * @param key
     * @return res
     */
	public byte[] oneTimePad(byte[] input,byte[] key,int size){
		byte[] res;
		if(input.length==key.length){
			res=new byte[size];
			for(int i=0;i<size;++i)
				res[i]= (byte) (input[i]^key[i]);
			return res;
		}
		else{
			System.out.println("error"+SIZE_ERROR);
			System.exit(0);
		}
		return null;
		
	}
	
	/***
	 * Generate a random key file with specified length
	 * @param keyFileName
	 * @param inputSize
	 */
	 
	public void generateKeyFile(String keyFileName,long inputSize){
		System.out.println(inputSize);
		final String ALLOWED_CHAR="abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789+@";
		Random rand = new Random();
	    BufferedOutputStream key = null;
		File keyFile = new File(keyFileName);
		/*Delete file if exists */
		if(keyFile.exists())
			keyFile.delete();
		else
			try {
				keyFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		try {
			key = new BufferedOutputStream(new FileOutputStream(new File(keyFileName),true)); //Taking random character from ALLOWED CHARACTER
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			/*Generate random string from Allowed Characters*/
			while (keyFile.length()!=inputSize+1){
				key.write(ALLOWED_CHAR.charAt(rand.nextInt(ALLOWED_CHAR.length()))); //Taking random character from ALLOWED CHARACTER and write to key file
				key.flush();
	        }
		    key.close();
		    System.out.println(KEY_MSG+keyFileName);
		    System.out.println("her"+keyFile.length());
		} catch (IOException e) {
			e.printStackTrace();
		}
	   
	}
	
}

