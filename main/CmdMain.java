/***
 * Command line frontend for cryptographic algorithms OTP,RSA and AES
 * General Usage:<algorithm_name> -e/-d <input_file> -k key -o <output_file>
 * 	Algorithms names are otp,rsa,des ,Use keyfile for RSA and OTP 
 *  To generate RSA key pair use rsa -g 
 * @author Jinesh M.K
 * Date: 20/1/13
 */
package pcs.main;


import java.io.File;

import pcs.aes.AESFile;
import pcs.otp.OTPFile;
import pcs.rsa.RSAFile;

public class CmdMain {
	
	private static final String INVALID_ARG = "Invalid Number of arguments";
	private static final String OTP="otp";
	private static final String RSA="rsa";
	private static final String AES="aes";
	private static final String ENC="-e";
	private static final String DEC="-d";
	private static final String KEY="-k";
	private static final String OUT="-o";
	private static final String OUTFILE="outfile.out";
	private static final String GEN_KEY = "-g";
	private static final String USAGE="General Usage:\n<algorithm_name> -e/-d <input_file> -k key -o <output_file>" +
    		"\n Algorithms names are otp,rsa,des ,Use keyfile for RSA and OTP " +
    		"\n To generate RSA key pair use rsa -g \n"+
    		"----------------------------------------------------------------------------------\n";
	
	boolean encFlag=false;
	private String inputFile,key,outputFile;
	/***
	 * Process Command line arguments 
	 * @param args
	 * @return 
	 */
	public void processCommand(String args[]){
		if(args.length>=2){
		/*To handle each algorithm */	
			if(args[0].equals(OTP))
				handleOTP(args);
			else if(args[0].equals(RSA))
				handleRSA(args);
			else if(args[0].equals(AES))
				handleAES(args);	
		}
		else
		{
			System.out.println(INVALID_ARG);
			System.exit(1);
		}
	}
	
	/****
	 *Handle AES options
	 * @param args
	 */
	private void handleAES(String[] args) {		
		AESFile aes=new AESFile();
		if(readOptions(args)){
			if(encFlag)
				aes.encryptFile(inputFile, key, outputFile);
			else
				aes.decryptFile(inputFile, key, outputFile);
		}
		else{
			System.err.println(INVALID_ARG);
			System.exit(1);
		}
		
	}
	
	/***
	 * Handle RSA options
	 * @param args
	 */
	private void handleRSA(String[] args) {	
		RSAFile rsa=new RSAFile();
		if(args[1].equals(GEN_KEY))
			rsa.generateKey(1024);
		else if(readOptions(args)){
			
			if(encFlag)
				rsa.encryptFile(inputFile, key, outputFile);
			else
				rsa.decryptFile(inputFile, key, outputFile);
		}
		else{
			System.err.println(INVALID_ARG);
			System.exit(1);
		}
		
	}
	/***
	 * Handle OTP options
	 * @param args
	 */
	private void handleOTP(String[] args) {		
		OTPFile otp=new OTPFile();
		if(readOptions(args)){
			if(encFlag){
				File keyFile=new File(key);
				if(!keyFile.exists())
					otp.generateKeyFile(key, new File(inputFile).length());
			}
			otp.encryptAndDecryptFile(inputFile, key, outputFile);
		}
		else{
			System.err.println(INVALID_ARG);
			System.exit(1);
		}
		
	}
	
	/***
	 * Read options from command line .It read input file,output file and key
	 * @param args
	 * @return true, if arguments are correct
	 */
	private boolean readOptions(String args[]){
		try{
			for(int i=0;i<args.length;++i){
				if(args[i].equals(ENC)){
					inputFile=args[i+1];
					encFlag=true;
				}
				else if(args[i].equals(DEC)){
					inputFile=args[i+1];
					encFlag=false;
				}
				else if(args[i].equals(KEY)){
					key=args[i+1];
				}
				else if(args[i].equals(OUT)){
					outputFile=args[i+1];
				}
			}
			if(inputFile!=null && key!=null){
				if(outputFile==null)
					outputFile=OUTFILE;
			    return true;
			}
			else{
				System.out.println("Error");
				System.err.println(INVALID_ARG);
				System.exit(1);
			}
		}catch(ArrayIndexOutOfBoundsException e){
			System.err.println(INVALID_ARG);
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	/**
	 * Main function
	 * @param args
	 */
	public static void main(String args[]){	
		CmdMain cmdmain=new CmdMain();
		System.out.print(USAGE);
		cmdmain.processCommand(args);
	}
}
