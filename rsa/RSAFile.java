
/***
 * Encrypt and decrypt the file using RSA
 * Use encryptFile(input File,public key file,output File) and decryptFile(input File,private key file ,output File) 
 * for encryption and decryption
 * @author Jinesh M.K
 * Date :20/1/2013
 */

package pcs.rsa;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.math.BigInteger;

public class RSAFile extends RSA{
	/*Messages */
	private static final String ENC_ERROR="Encryption Error";
	private static final String DEC_ERROR="Decryption Error";
	private static final String ENC_MESSAGE="File Encrypted and stored in ";
	private static final String DEC_MESSAGE="File Decrypted and stored in ";
	BufferedReader fileIn = null;
	BufferedWriter fileOut=null;

	/***
	 * Encrypt a inputFile using key and stored to outputFile 
	 * @param inputFileName
	 * @param key
	 * @param outputFileName
	 */
	public void encryptFile(String inputFileName,String publicKeyfile,String outputFileName){	
		try{		
			File inputFile=new File(inputFileName);
			fileIn=new BufferedReader(new FileReader(inputFile));
			File outputFile = new File(outputFileName);
			outputFile.createNewFile();
			FileWriter fileOut = new FileWriter(outputFile,false);
			/*Read data from file encrypt and store to a file*/
			String line=null;
			while((line=fileIn.readLine())!=null){//Read line by line
				if(line.length()==0)
				{
					fileOut.write(10);
					
				}
				else
				{
					BigInteger text = new BigInteger(line.getBytes());// Convert line to integer
					text=encrypt(text, publicKeyfile); //Encrypt the data
					fileOut.write(text.toString(), 0, text.toString().length()); //Write to file
					fileOut.write(10); //Write new line to file
				}
			}
			System.out.println(ENC_MESSAGE+outputFileName);
			fileIn.close();
			fileOut.close();
		}catch(Exception e)
		{
			System.err.println(ENC_ERROR);
			e.printStackTrace();
		}
	}
	
	/***
	 * Decrypt a inputFile using key and stored to the outputFile 
	 * @param inputFileName
	 * @param key
	 * @param outputFileName
	 */
	public void decryptFile(String inputFileName,String privatekeyFile,String outputFileName){
		try{
			File inputFile=new File(inputFileName);
			fileIn=new BufferedReader(new FileReader(inputFile));
			File outputFile = new File(outputFileName);
			outputFile.createNewFile();
			FileWriter fileOut = new FileWriter(outputFile,false);

			/*Read data from file encrypt and store to a file*/
			String line=null;
			while((line=fileIn.readLine())!=null){ //Take data line by line
				if(line.length()==0)
				{
					fileOut.write(10);
					
				}
				else
				{
					BigInteger text = new BigInteger(line);
					text=decrypt(text, privatekeyFile); //decrypt the line
					String tmp = new String(text.toByteArray());// Convert to string 
					fileOut.write(tmp, 0, tmp.length()); //Write to output file
					fileOut.write(10);                   //Add new line
				}
			}
			System.out.println(DEC_MESSAGE+outputFileName);
			fileIn.close();
			fileOut.close();
		}catch(Exception e)
		{
			System.err.println(DEC_ERROR);
			e.printStackTrace();
		}
	}
}
