/***
 * RSA implementation 
 * RSA is an algorithm for public-key cryptography that is based 
 * on the presumed difficulty of factoring large integers, the factoring problem
 * Author: Jinesh M.K
 * Date: 19/1/13
 */

package pcs.rsa;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.math.BigInteger;
import java.security.SecureRandom;

class RSA{

	 
	  private BigInteger n, d, e; //To store private and public key
	  private static final String FILE_ERROR="Unable to write key file";
	  private static final String KEY_ERROR="Unable to read key file";
	  private static final String PUBLIC_KEY="~/rsa/id_rsa.pub"; //public key filename
	  private static final String PRIVATE_KEY="~/rsa/id_rsa";   //private key file name
	  private static final String PUB_STORE = "Public key stored at ";
	  private static final String PRI_STORE = "Private key stored at ";
      
	  
	  /***
	   * Generate public and private key and store in the file
	   * @param bitLen
	   */
	  public void generateKey(int bitLen){
		    SecureRandom r = new SecureRandom(); //Select a random number
		    /*Select a random p and q */
		    BigInteger p = new BigInteger(bitLen / 2, 100, r);
		    BigInteger q = new BigInteger(bitLen / 2, 100, r);
		    /* n=p*q */
		    n = p.multiply(q);
		    /* m=(p-1)*(q-1) */
		    BigInteger m = (p.subtract(BigInteger.ONE))
		                   .multiply(q.subtract(BigInteger.ONE));
		    /*public key calculation */
		    e = new BigInteger(bitLen / 2, 100, r);
		    /*Determine d as d ≡ e−1 (mod φ(n)), i.e., d is the multiplicative inverse of e (modulo φ(n))*/
		    while(m.gcd(e).intValue() > 1) e = e.add(new BigInteger("1"));
		    	d = e.modInverse(m);
		   writeKeyFile(PRIVATE_KEY, n.toString(), d.toString()); 
		   System.out.println(PRI_STORE+PRIVATE_KEY);
		   writeKeyFile(PUBLIC_KEY, n.toString(), e.toString());    		    		    
		   System.out.println(PUB_STORE+PUBLIC_KEY); 
	  }
	  /***
	   * Write key to file
	   * @param file
	   * @param val1
	   * @param val2
	   */
	  private void writeKeyFile(String file,String val1,String val2){
		  try{
			  /*Remove public and private key file if exists */
			    File keyFile=new File(file);
			  	BufferedWriter keyWrite;
			    if(keyFile.exists())
			    	keyFile.delete();
		    	/*Write  key to file*/
		    	keyWrite=new BufferedWriter(new FileWriter(file,true));
		    	keyWrite.write(val1);
		    	keyWrite.newLine();
		    	keyWrite.write(val2);
		    	keyWrite.flush();
		    	keyWrite.close();
		    	
		  }catch(Exception e){
		    	System.err.println(FILE_ERROR);
		    	e.printStackTrace();	    	
		    }
	  }
	  
	  /****
	   * Encrypt a message using public key
	   * @param message-plain text
	   * @param public key file
	   * @return cipher text
	   */
	  public BigInteger encrypt(BigInteger message,String publicKeyfile){
		  try {
			   BufferedReader keyFile=new BufferedReader(new FileReader(publicKeyfile));
			   n=new BigInteger(keyFile.readLine());
			   e=new BigInteger(keyFile.readLine());
			   keyFile.close();
		  } catch (Exception e) {
			  System.err.println(KEY_ERROR);
			  e.printStackTrace();
		  }
		    /* c=(m^e) mod n */
		  return message.modPow(e, n);
	  }
	  
	  /****
	   * decrypt a message using private key
	   * @param message- cipher text
	   * @param private key file
	   * @return plain text
	   */
	  public BigInteger decrypt(BigInteger message,String privateKeyfile){
		  try {
			    BufferedReader keyFile=new BufferedReader(new FileReader(privateKeyfile));
			    n=new BigInteger(keyFile.readLine());
			    d=new BigInteger(keyFile.readLine());
			    keyFile.close();
		  } catch (Exception e) {
			  System.err.println(KEY_ERROR);
			  e.printStackTrace();
		  }
		  /* p=(m ^d ) mod n */
		  return message.modPow(d, n);
	  }

}
