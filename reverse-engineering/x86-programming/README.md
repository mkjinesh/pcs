Introduction to x86 Assembly programming using nasm
===================================================

This is the git repository of all files related to the introductory class on
x86 assembly programming using nasm. The slides folder includes files of the
slides used in the class and the assignments folder contains the questions for
the assignment.

Compiling using nasm
--------------------

1. Generate object code.
    ``` bash
            nasm -f elf filename.asm
    ```

2. Compile the object code to create executable.
    ``` bash
            gcc filename.o -o filename.out
    ```
    If you are running a 64 bit version of Ubuntu, you will have to install
    the package gcc-multilib and pass '-m32' to gcc to generate 32 bit binary.
