;-----------------------------------------------------------------------------;
;                                                                             ;
;   Author: Jinesh M.K                                                        ;
;   Date:   29/1/13                                                           ;
;   Filename:  soln4.asm                                                      ;
;   Program:  program that performs conversion between Celsius scale and      ;                                                                                                   
;             Fahrenheit scale                                                ;
;             Tf = (9/5)*Tc+32                                                ; 
;             Tc = (5/9)*(Tf-32); 					      ;
;	   Tc = temperature in degrees Celsius, Tf = temperature in degrees       ;
;-----------------------------------------------------------------------------;


section .data
     ;messages to print
     prompt : db "Temperature Conversion",10,0
     option : db "1: Degrees Celsius to Fahrenheit 2:Fahrenheit to Degrees Celsius",10,0
     message: db "Enter your choice:",0
     inmessage: db "Enter the value to convert:",0
     F: db " F",10,0
     C: db " C",10,0
     invalid: db "Invalid Option",10,0
     ;input and output format
     fmt1: db "%lf", 0
     fmt2: db 10, "The converted value is: %lf", 0
     fmt3: db "%d",0
     ;constants in computation
     tcconst: dq 0.555555556
     tfconst: dq 1.8 
     conval: dq 32.0
     ;variable to read
     option_val: dq 0
     
section .text
    global main
    extern scanf
    extern printf
    main:
        push ebp
        mov ebp, esp
     
        ;show prompt message
        push prompt                     ; push message to stack
        call printf                     ; call printf
        add esp,4                       ; remove prinf parameter
        push option                     ; push option message to stack
        call printf                     ; call printf
        add esp,4                       ; remove prinf parameter
        push message                    ; push option message to stack
        call printf                     ; call printf
        add esp,4                       ; remove prinf parameter

        ;Read option  
        push option_val                 ; address of option
        push fmt3                       ; scanf format string
        call scanf
        add esp,8                       ; remove scanf parameter  
        mov ecx,[option_val]
        mov edx,0x1                     ; move 18 to ecx
        cmp edx,ecx                     ; comapare input with '1'
        je CtoF 
        mov edx,0x2                     ; move 18 to ecx
        
        cmp edx,ecx                     ; comapare input with '2'
        je FtoC
        
        push invalid                    ;Push invalid option
        call printf
        add esp,4
        jmp exit
        FtoC:
        push inmessage
        call printf
            add esp,4

            sub esp, 8                  ;Allocate space to read val
            lea ecx, [ebp - 8]          ;load effective address to location to read
            push ecx
            push fmt1                   ;
            call scanf                  ;scan float value
            add esp, 8                  ;Remove parameter from stack
            ;Perform the conversion 
            fld qword [ecx]             ;load to st0
            fld qword [conval]          ;load 32 to st0
            fsubp st1,st0               ;st1=st1-st0
            fmul qword [tcconst]        ;multiply with tcconst and result in st0
       
         ;print value to output
            fst qword [ebp - 8]         ;store the output
            push fmt2                   ;format string
            call printf
            add esp, 12                 ;print unit
            push C
            call printf
            add esp,4
            jmp exit
        CtoF:
            push inmessage
            call printf
            add esp,4

            sub esp, 8                  ;Allocate space to read val
            lea ecx, [ebp - 8]          ;load effective address to location to read
            push ecx
            push fmt1                   ;
            call scanf                  ;scan float value
            add esp, 8                  ;Remove parameter from stack
            ;Perform the conversion 
            fld qword[conval]
            fld qword [ecx]
            fmul qword [tfconst]        ;multiply with tfconst
            fadd st0,st1
            ;print value to output
            fst qword [ebp - 8]         ;store the output
            push fmt2                   ;format string
            call printf
            add esp, 12                 ;print unit
            push F
            call printf
            add esp,4
            jmp exit
        exit:
            pop ebp                         ; restore ebp
            mov eax, 0                      ; exit code
            ret
;----------------------------------------------------------------------------------------;

