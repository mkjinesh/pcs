;-----------------------------------------------------------------------------;
;                                                                             ;
;   Author: Jinesh M.K                                                        ;
;   Date:   29/1/13                                                           ;
;   Filename:  soln5.asm                                                      ;
;   Program :Simple calculator                                                ;
;-----------------------------------------------------------------------------;


section .data
     ;messages to print
     prompt : db "-----------------------Simple Calculator-------------------------------",10,0
     oprtn  : db "Enter operation : 1. Addition 2. Subtraction 3. Left shift 4. Right shift 5. Binary and 6. Binary or 7. Binary not ",10,0
     enterno: db "Enter the number",10,0 
     invalid: db "Invalid entry",10,0
     contmsg: db "Press 1 to continue",10,0
     ;variable to read value
     input1 : dd 0
     input2 : dd 0
     result : dd 0
     opr    : dd 0    
     ;scanf and printf reading format
     intfmt : db "%d",0
     intfmt2: db "Result: %d",10,0

section .text
    global main
    extern scanf
    extern printf
    main:
        push ebp                        ;Save ebp
        mov ebp, esp

        ;show prompt message
        push prompt                     ; push message to stack
        call printf                     ; call printf
        add esp,4                       ; remove printf parameter
    
      entry: 
	call show_msg                    ;show the message
     
        push input1                     ;Read 1st input from user
        push intfmt                     ;Read format %d
        call scanf  
        add esp,8                       ;Remove parameters

        push oprtn                      ;push operation message
        call printf
        add esp,4
        
	push opr                        ;Read operation number from user
        push intfmt
        call scanf
        add esp,8
    	  
        mov ecx,[opr]                   ;move operation number to ecx
	;For add
        cmp ecx,0x1                     ;Comapre with 1 for sum
        je add_val                      ;go to add_val section if input=1
	;for sub
        cmp ecx,0x2
        je sub_val
	;For left shift
        cmp ecx,0x3
        je left_shift
        ;right shift
        cmp ecx,0x4
        je right_shift
	;For logical and
        cmp ecx,0x5
        je bin_and
        ;for logical or
	cmp ecx,0x6
        je bin_or
        ;logical not
        cmp ecx,0x7
        je bin_not

      	jmp invaliden

        add_val:                      ;To add number
          call show_msg
          push input2                 ;second number to read
          push intfmt                 ;format string
          call scanf                  ;read
          add esp,8                   ;remove parameters
         
          mov eax, [input1]           ;copy content to register      
          mov edx, [input2] 
          add eax, edx                ;add two register
          
                                      ;show answer
          push dword eax              
          push intfmt2
          call printf
          add esp,8
          jmp exit

       	sub_val:                       ;to substract         
          call show_msg
          push input2       
          push intfmt
          call scanf
          add esp,8
        
          mov eax,[input1] 
          mov edx,[input2]
          sub eax, edx 
          
          push dword eax
          push intfmt2
          call printf
          add esp,8
       	  jmp exit

       	left_shift:                    ;left shift
          mov edx,[input1]
          mov ecx,0x1
          shl dl,1                    ;left shift by one
          push edx
          push intfmt2
          call printf
          add esp,8
	  jmp exit
       
       	right_shift:                   ;right shift
          mov edx,[input1]            
          mov ecx,0x1                 ;right shift by one
          shr dl,1
          push edx
          push intfmt2
          call printf
          add esp,8
	  jmp exit
	bin_and:                        ;binary and
          call show_msg
	  push input2                 ;read second number        
          push intfmt
          call scanf
          add esp,8           
        
	  mov eax,[input1]
	  mov edx,[input2]
	  and eax,edx  
          
          push dword eax
          push intfmt2
          call printf
          add esp,8
       	  jmp exit

        bin_or:                        ;binary or
          call show_msg
	        push input2
          push intfmt
          call scanf
          add esp,8
        
	  mov eax,[input1]
	  mov edx,[input2]
	  or eax,edx  
          
          push dword eax
          push intfmt2
          call printf
          add esp,8
       	  jmp exit
     
        bin_not:                       ;binay not
	        
	  mov eax,[input1]
	  not eax 
          
          push dword eax
          push intfmt2
          call printf
          add esp,8
       	  jmp exit
       invaliden:
          push invalid
          call printf
          add esp,4
          jmp exit

       show_msg:                               ;show enter number function
          push ebp
          mov ebp,esp
          push enterno
          call printf
          add esp,4
          pop ebp      
          ret
       exit:
	  push contmsg
	  call printf
	  add esp,4
	  push opr                        ;Read operation number from user
          push intfmt
          call scanf
          add esp,8
    	  
          mov ecx,[opr]                   ;move operation number to ecx
          cmp ecx,0x1                     ;Comapre with 1 for sum
          je entry     
          pop ebp                         ; restore ebp
          mov eax, 0                      ; exit code
          ret:

  
