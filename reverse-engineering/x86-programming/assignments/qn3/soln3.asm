;-----------------------------------------------------------------------------;
;                                                                             ;
;       Author: Jinesh M.K                                                    ;
;       Date:   29/1/13                                                       ;
;       Filename:  soln3.asm                                                  ;
;       Program:  Program that receives the age of a person as input and      ;
;                    outputs whether the person is eligible for voting or not.;
;-----------------------------------------------------------------------------;

section .data
    prompt:  db "Enter age", 10, 0
    msg1:    db "You are eligible for voting", 10, 0      ; msg1 to print                   		 
    msg2:    db "You are not eligible for voting", 10, 0  ; msg2 to print
    age:     times 4 db 0                                 ; 32-bits integer 
    formatint: db "%d", 0                                 ; scanf format string

section .text
    global main                         ;Entry point for gcc
    extern scanf 
    extern printf
    main:
        push ebp                        ; saving old base pointer
        mov ebp, esp                    ; setting up current base pointer
        
        ;show prompt message
        push prompt                     ; push message to stack
        call printf                     ; call printf
        add esp,4                       ; remove prinf parameter
        
        ;read age from user
        push age                        ; address of age
        push formatint                  ; scanf format string
        call scanf
        add esp,8                       ; remove scanf parameter
        
        ;comapare the input value
        mov edx,[age]                   ; move age value to edx
        mov ecx,0x12                    ; move 18 to ecx
        cmp edx,ecx                     ; comapare input with 18
        jge eligible                    ; goto label eligible >=18
        
        push msg2                       ; push not eligible msg
        call printf                     ; print message
        jmp end

        eligible:
         push msg1                      ; push eligble message
         call printf                    ; print message
    
        end:
         add esp,4                      ; Remove printf message
         pop ebp                        ; restore ebp
         mov eax, 0                     ; exit code
         ret
;------------------------------------------------------------------------------;
