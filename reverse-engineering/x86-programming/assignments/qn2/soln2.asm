;-----------------------------------------------------------------------------;
;                                                                             ;
;       Author: Jinesh M.K                                                    ;
;       Date:   29/1/13                                                       ;
;       Filename:  soln3.asm                                                  ;
;       Program: simple hello world program in x86 assembly using system call ;
;                    					      		      ;
;-----------------------------------------------------------------------------;

section .data
    msg:     db "Hello World", 10, 0       ;Message to print
    msglen:  equ $-msg
  
section .text
    global main                 ;Entry point for gcc

    main:
	mov edx, msglen     	; The length of the string
        mov ecx, msg            ; The address of the string
        mov ebx, 1              ; stdout,here it is screen
        mov eax, 4              ; Syscall number for write()
        int     0x80            ; Interrupt 0x80    
        mov eax, 0              ; exit code
        ret
;------------------------------------------------------------------------------;
