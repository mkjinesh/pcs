#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
    char ans[] = "reversing";
    if(argc != 4) {
        printf("What are you trying to do?\n");
        return 1;
    }

    if(atoi(argv[3]) != 0x1337) {
        printf("You are not l33t!\n");
        return 2;
    }

    if(strcmp(argv[2], "is") != 0) {
        printf("Boo! Go away!\n");
        return 3;
    }

    int i, len = strlen(ans);
    for(i = 0; i < (len - 1); i++) {
        if(argv[1][i] != ans[i]) {
            printf("Almost there!\n");
            return 4;
        }
    } 
    printf("Congratulations! You found all the parameters!\n");
    return 0;
}
