/***
 * Encrypt and decrypt the file using AES
 * Use encryptFile(input File ,key,outputFile) and decryptFile(inputFile ,key,outputFile ) 
 * for encryption and decryption
 * @author Jinesh M.K
 * Date :20/1/2013
 */

package pcs.aes;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class AESFile extends AES{
	/*Messages */
	private static final String ENC_ERROR="Encryption Error";
	private static final String DEC_ERROR="Decryption Error";
	private static final String ENC_MESSAGE="File Encrypted and stored in ";
	private static final String DEC_MESSAGE="File Decrypted and stored in ";
	
	/***
	 * Encrypt a inputFile using key and stored to outputFile 
	 * @param inputFileName
	 * @param key
	 * @param outputFileName
	 */
	public void encryptFile(String inputFileName,String key,String outputFileName){	
		BufferedInputStream  fileIn = null;
		BufferedOutputStream fileOut=null;
		
		try{
			
			File inputFile=new File(inputFileName);
			fileIn=new BufferedInputStream(new FileInputStream(inputFile));
			fileOut = new BufferedOutputStream(new FileOutputStream(outputFileName,true));
			/*Read data from file */
			byte[] input=new byte[(int) inputFile.length()]; //Read entire file
			fileIn.read(input);		
			/* Encrypt and store the data */
			fileOut.write(encryptData(input, key.getBytes()));
			fileOut.flush();			
			System.out.println(ENC_MESSAGE+outputFileName);
			fileIn.close();
			fileOut.close();
		}catch(Exception e)
		{
			System.err.println(ENC_ERROR);
			e.printStackTrace();
		}
	}
	
	/***
	 * Decrypt a inputFile using key and stored to the outputFile 
	 * @param inputFileName
	 * @param key
	 * @param outputFileName
	 */
	public void decryptFile(String inputFileName,String key,String outputFileName){
		BufferedInputStream  fileIn = null;
		BufferedOutputStream fileOut=null;
		
		try{
			File inputFile=new File(inputFileName);
			fileIn=new BufferedInputStream(new FileInputStream(inputFileName));
			fileOut = new BufferedOutputStream(new FileOutputStream(outputFileName,true));
			
			/*Read data from file */
			byte[] input=new byte[(int) inputFile.length()];
			fileIn.read(input);	
			
			/* Decrypt and store the data */
			fileOut.write(decryptData(input, key.getBytes()));
			fileOut.flush();			
			System.out.println(DEC_MESSAGE+outputFileName);
			fileIn.close();
			fileOut.close();
		}catch(Exception e)
		{
			System.err.println(DEC_ERROR);
			e.printStackTrace();
		}
	}
}
